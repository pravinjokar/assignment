//
//  HomeVCVM.swift
//  Assignment
//
//  Created by Pravin on 24/03/22.
//

import Foundation
import CoreData
import UIKit

class HomeVCVM : NSObject {
    
    var homeDataArr = [HomeDataModel]()
    
    override init() {
        super.init()
    }
    
    func performFetchOperations(completion: @escaping (_ success: Bool, _ data: [HomeDataModel]) -> Void) {
       
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Details")
        //request.returnsObjectsAsFaults = false
        
        do {
            
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
                let homedata = HomeDataModel()
                if let title = data.value(forKey: "title") as? String {
                    print(data.value(forKey: "title") as! String)
                    homedata.title = title
                }
                if let description = data.value(forKey: "descriptionvalue") as? String {
                    print(data.value(forKey: "descriptionvalue") as! String)
                    homedata.descriptionvalue = description
                }
                
                homedata.object = data
                
                self.homeDataArr.append(homedata)
            }
            
            completion(true, self.homeDataArr)

        } catch {
            print("Failed")
            completion(false, self.homeDataArr)
        }
    }
    func performSaveOperations(data: HomeDataModel, completion: @escaping (_ success: Bool, _ data: [HomeDataModel]) -> Void) {
        
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let entity =
        NSEntityDescription.entity(forEntityName: "Details",
                                   in: managedContext)!
        
        let details = NSManagedObject(entity: entity,
                                      insertInto: managedContext)
        
        details.setValue(data.title, forKeyPath: "title")
        details.setValue(data.descriptionvalue, forKeyPath: "descriptionvalue")
        
        do {
            try managedContext.save()
            //self.homeDataArr.append(data)
            completion(true, self.homeDataArr)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completion(false, self.homeDataArr)
        }
    }
    func performRenameOperations(title: String, description: String, data: HomeDataModel, completion: @escaping (_ success: Bool) -> Void) {
        
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let details = data.object
        if let title = details.value(forKey: "title") as? String {
            print(details.value(forKey: "title") as! String)
            details.setValue(title, forKey: "title")
        }
        
        details.setValue(description, forKey: "descriptionvalue")
        
        do {
            try managedContext.save()
            completion(true)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completion(false)
        }
    }
    func performDeleteOperations(data: HomeDataModel, completion: @escaping (_ success: Bool) -> Void) {
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        do {
            
            managedContext.delete(data.object)
            
            try managedContext.save()
            
            completion(true)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completion(false)
        }
    }
}
