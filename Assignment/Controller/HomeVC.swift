//
//  HomeVC.swift
//  Assignment
//
//  Created by Pravin on 23/03/22.
//

import UIKit

class HomeVC: UIViewController {
    
    
    @IBOutlet var vwScrollPager: ScrollPager!
    @IBOutlet weak var vwPageContainer: UIView!
    
    var pageViewController: UIPageViewController?
    var arrAllViews = [UIViewController]()
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Home"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupPageViewFrame()
        self.setScrollPager()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension HomeVC {
    func setScrollPager() {
        self.vwScrollPager.objScrollPagerDelegate = self
          
        weak var objTodayVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TodayVC") as? TodayVC ?? TodayVC()
        self.arrAllViews.append(objTodayVC ?? UIViewController())

        weak var objTomorrowVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TomorrowVC") as? TomorrowVC ?? TomorrowVC()
        self.arrAllViews.append(objTomorrowVC ?? UIViewController())

        weak var objUpcomingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpcomingVC") as? UpcomingVC ?? UpcomingVC()
        self.arrAllViews.append(objUpcomingVC ?? UIViewController())
        
        self.vwScrollPager.addSegmentsWithTitles(segmentTitles: ["Today", "Tomorrow", "Upcoming"])
        
        if let firstVC = self.arrAllViews.first {
            self.pageViewController?.setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
        }
    }
    func setupPageViewFrame() {
        self.pageViewController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        self.pageViewController?.dataSource = self
        self.pageViewController?.delegate = self
        self.pageViewController?.view.frame = CGRect.init(x: self.vwPageContainer.frame.origin.x, y: 0, width: self.vwPageContainer.frame.size.width, height: self.vwPageContainer.frame.size.height)//self.vwPageContainer.frame
        self.vwPageContainer.addSubview(pageViewController?.view ?? UIView())
    }
}

extension HomeVC: ScrollPagerDelegate {
    func ScrollPager(scrollPager: ScrollPager, changedIndex: Int) {
        if changedIndex < self.arrAllViews.count  {
            self.pageViewController?.setViewControllers([self.arrAllViews[changedIndex]], direction: changedIndex >= self.selectedIndex ? .forward : .reverse, animated: false, completion: nil)
        }
        self.selectedIndex = changedIndex
    }
}
extension HomeVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = arrAllViews.firstIndex(of: viewController) else { return nil }
        let previousIndex = currentIndex - 1
        return (previousIndex == -1) ? nil : arrAllViews[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = arrAllViews.firstIndex(of: viewController) else { return nil }
        let nextIndex = currentIndex + 1
        return (nextIndex == arrAllViews.count) ? nil : arrAllViews[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed, let viewControllerIndex = arrAllViews.firstIndex(of: pageViewController.viewControllers![0]) else {
            return
        }
        self.selectedIndex = viewControllerIndex
        self.vwScrollPager.setSelectedIndex(index: self.selectedIndex, animated: true)
    }
}
