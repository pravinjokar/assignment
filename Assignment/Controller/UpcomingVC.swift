//
//  UpcomingVC.swift
//  Assignment
//
//  Created by Pravin on 25/03/22.
//

import UIKit

class UpcomingVC: UIViewController {

    @IBOutlet var tblHomeData: UITableView!
    @IBOutlet var btnAdd: UIButton!
    
    var objHomeVCVM = HomeVCVM()

    var homeDataArray = [HomeDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.objHomeVCVM.performFetchOperations { success, data in
            if success {
                self.homeDataArray = data
            }
        }
        
        self.btnAdd.layer.cornerRadius = 25
        
        self.registerTableViewCell()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpcomingVC {
    func registerTableViewCell() {
        self.tblHomeData.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
    }
    @IBAction func btnAddClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Add Task Title and Description", message: "", preferredStyle: .alert)
        alert.addTextField { textfield in
            textfield.placeholder = "Task Title"
        }
        alert.addTextField { textfield in
            textfield.placeholder = "Task Description"
        }
        
        let submitAction = UIAlertAction(title: "Add", style: .default) { [unowned alert] _ in
            var description = ""
            let homedata = HomeDataModel()
            
            if let textfield = alert.textFields?[1] {
                let answer = textfield.text ?? ""
                if answer.trimmingCharacters(in: .whitespaces).count != 0
                {
                    print("task Description : \(answer ?? "")")
                    description = answer
                } else {
                    print("enter blank value")
                }
            }
            
            if let textfield = alert.textFields?[0] {
                let answer = textfield.text ?? ""
                if answer.trimmingCharacters(in: .whitespaces).count != 0
                {
                    print("task Title : \(answer ?? "")")
                    homedata.title = answer
                    homedata.descriptionvalue = description
                    
                    self.objHomeVCVM.performSaveOperations(data: homedata) { success, data in
                        if success {
                            //self.homeDataArray.removeAll()
                            //self.homeDataArray = data
                            //self.tblHomeData.reloadData()

                            self.objHomeVCVM.performFetchOperations { success, data in
                                self.homeDataArray.removeAll()
                                self.homeDataArray = data
                                self.tblHomeData.reloadData()
                            }
                        }
                    }
                    
                } else {
                    print("enter blank value")
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [unowned alert] _ in
            print("cancel click")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        present(alert, animated: true)
    }
    
    @objc func rename(sender: UIButton) {
        let alert = UIAlertController(title: "Rename Task Title and Description", message: "", preferredStyle: .alert)
        alert.addTextField { textfield in
            textfield.placeholder = "Task Title"
        }
        alert.addTextField { textfield in
            textfield.placeholder = "Task Description"
        }
        
        let submitAction = UIAlertAction(title: "Confirm", style: .default) { [unowned alert] _ in
            
            var title = ""
            var description = ""
            let homedata = HomeDataModel()
            
            if let textfield = alert.textFields?[1] {
                let answer = textfield.text ?? ""
                if answer.trimmingCharacters(in: .whitespaces).count != 0
                {
                    print("task Description : \(answer ?? "")")
                    description = answer
                } else {
                    print("enter blank value")
                }
            }
            
            if let textfield = alert.textFields?[0] {
                let answer = textfield.text ?? ""
                if answer.trimmingCharacters(in: .whitespaces).count != 0
                {
                    print("task Title : \(answer ?? "")")
                    if self.homeDataArray.count > sender.tag {
                        self.objHomeVCVM.performRenameOperations(title: answer, description: description, data: self.homeDataArray[sender.tag]) { success in
                            if success {
                                self.homeDataArray[sender.tag].descriptionvalue = description
                                self.homeDataArray[sender.tag].title = answer
                                self.tblHomeData.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
                            }
                        }
                    }
                } else {
                    print("enter blank value")
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [unowned alert] _ in
            print("cancel click")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        present(alert, animated: true)
    }
    @objc func delete(sender: UIButton) {
        print("delete called")
        if self.homeDataArray.count > sender.tag {
            self.objHomeVCVM.performDeleteOperations(data: self.homeDataArray[sender.tag]) { success in
                if success {
                    self.homeDataArray.remove(at: sender.tag)
                    self.tblHomeData.reloadData()
                }
            }
        }
    }
}
extension UpcomingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeDataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell {
            cell.lblTitle.text = self.homeDataArray[indexPath.row].title
            cell.lblDescription.text = self.homeDataArray[indexPath.row].descriptionvalue
            
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(rename(sender:)), for: .touchUpInside)
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(delete(sender:)), for: .touchUpInside)

            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
